﻿using Model;
using Persistence;
using System.Collections.Generic;
using System.Linq;

namespace Service
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetAll();
        Customer Get(int id);
        bool Add(Customer model);
        bool Update(Customer model);
        bool Delete(int id);
    }

    public class CustomerService : ICustomerService
    {
        private readonly CustomerDbContext _customerDbContext;

        public CustomerService(CustomerDbContext customerDbContext)
        {
            _customerDbContext = customerDbContext;
        }

        public IEnumerable<Customer> GetAll()
        {
            List<Customer> result = new List<Customer>();

            try
            {
                result = _customerDbContext.Customers.ToList();
            }

            catch (System.Exception)
            {
                
            }

            return result;
        }

        public Customer Get(int id)
        {
            Customer result = new Customer();

            try
            {
                result = _customerDbContext.Customers.Single(x => x.Id == id);
            }

            catch (System.Exception)
            {

            }

            return result;
        }

        public bool Add(Customer model)
        {
            try
            {
                _customerDbContext.Add(model);
                _customerDbContext.SaveChanges();
            }

            catch (System.Exception)
            {
                return false;
            }

            return true;
        }

        public bool Update(Customer model)
        {
            try
            {
                Customer originalModel = _customerDbContext.Customers.Single(x => x.Id == model.Id);

                originalModel.Names = model.Names;
                originalModel.Surnames = model.Surnames;
                originalModel.BirthDate = model.BirthDate;

                _customerDbContext.Update(originalModel);
                _customerDbContext.SaveChanges();
            }

            catch (System.Exception)
            {
                return false;
            }

            return true;
        }

        public bool Delete(int id)
        {
            try
            {
                _customerDbContext.Entry(new Customer { Id = id }).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _customerDbContext.SaveChanges();
            }

            catch (System.Exception)
            {
                return false;
            }

            return true;
        }
    }
}
