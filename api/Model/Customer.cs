﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Customer
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Names { get; set; }

        [MaxLength(50)]
        public string Surnames { get; set; }

        public DateTime BirthDate { get; set; }
    }
}
