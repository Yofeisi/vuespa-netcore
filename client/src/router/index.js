import Vue from 'vue'
import Router from 'vue-router'

import Default from '@/components/Default'
import ExampleIndex from '@/components/example/Index'
import ExampleView from '@/components/example/View'
import CustomerIndex from '@/components/customers/Index'
import CustomerCreateOrUpdate from '@/components/customers/CreateOrUpdate'

Vue.use(Router)

const routes = [
  { path: '/', name: 'Default', component: Default },
  { path: '/example', name: 'ExampleIndex', component: ExampleIndex },
  { path: '/example/:id', name: 'ExampleView', component: ExampleView },
  { path: '/customers', name: 'CustomerIndex', component: CustomerIndex },
  { path: '/customers/add', name: 'CustomerCreate', component: CustomerCreateOrUpdate },
  { path: '/customers/:id/edit', name: 'CustomerEdit', component: CustomerCreateOrUpdate },
]

export default new Router({
  routes
})
