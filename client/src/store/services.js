import Axios from 'axios'
import exampleService from '../services/ExampleService'
import customerService from '../services/CustomerService'

let apiUrl = 'https://localhost:44354/'

// Axios Configuration
Axios.defaults.headers.common.Accept = 'application/json'

export default {
    exampleService: new exampleService(Axios),
    customerService: new customerService(Axios, apiUrl)
}